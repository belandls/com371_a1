#pragma once
#include"glfw3.h"
class MouseManager
{
private:
	
	static bool _zoom_activated;
	static int _buffered_difference;
	static GLFWwindow* _current_window;
	static double _base_x;
	static double _base_y;

	//callbacks
	static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	static void MousePositionCallback(GLFWwindow* window, double xpos, double ypos);

	MouseManager(GLFWwindow* currentWindow);

public:
	
	//~MouseManager();

	static void AttachCallbacks(GLFWwindow* currentWindow);

	static int GetAndClearBufferedDifference();
};

