/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Helper class to load the vertex and fragment shader from file, compile them and link them
*/
#pragma once
#include<string>
#include "glew.h"

using namespace std;

class Shader
{
private:
	GLuint _current_program;

public:
	Shader();
	~Shader();

	bool LoadAndCompileShaders(string vertex, string fragment);

	GLuint GetCurrentProgram() { return _current_program; }
};

