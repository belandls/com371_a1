/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : GLFW/OpenGL initialization + main render/input loop
*/

#include "glew.h"	
#include "glfw3.h"	
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "Shader.h"
#include "MouseManager.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#define cimg_use_jpeg
#include "CImg.h"
#include "InputManager.h"
#include <vector>
#include "ImageDataExtractor.h"

using namespace std;

// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 800;



// The MAIN function, from here we start the application and run the game loop
int main()
{
	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Assignment 1 : Heightmap", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	// Define the viewport dimensions (and stores the frambuffer size)
	InputManager::SetInitialWindowSize(window);

	//Enable the depth test to remove hidden surfaces
	glEnable(GL_DEPTH_TEST);
	//Make the points bigger so that they are easier to see
	glEnable(GL_PROGRAM_POINT_SIZE);
	glPointSize(2);

	//Attach callbacks for inputs
	InputManager::AttachCallbacks(window);

	Shader s;
	//Load the heaightmap data, the color data, and calculates a 2x interpolation
	ImageDataExtractor imgExt("Images/hm.jpg", "Images/col.jpg");
	if (!imgExt.IsDataLoaded()){
		return 2;
	}

	std::cout << "Images (Heightmap and color) loaded sucessfully" << std::endl;

	//Initilaize the main VAO and the VBOs for color/position/index then sends the data to openGL
	imgExt.LoadDataInOpenGL();


	glm::mat4 projection;

	//Use model matrix to center the model to the world's origin


	//Try to compile the shaders and link them
	if (s.LoadAndCompileShaders("vertex.shader", "fragment.shader")) {


		while (!glfwWindowShouldClose(window)) {
			glfwPollEvents();

			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(s.GetCurrentProgram());

			//Regenerate the model and view matrix since it is very likely to change
			glm::mat4 model;
			glm::mat4 view;

			//Apply the translation "after" the rotation in oder to rotate around the center of the field
			//Apply the mesh rotations
			model = glm::rotate(model, glm::radians(InputManager::GetCurrentXMeshRotationModifier()), glm::vec3(1.0f, 0.0f, 0.0f));
			model = glm::rotate(model, glm::radians(InputManager::GetCurrentYMeshRotationModifier()), glm::vec3(0.0f, 1.0f, 0.0f));
			model = glm::rotate(model, glm::radians(InputManager::GetCurrentZMeshRotationModifier()), glm::vec3(0.0f, 0.0f, 1.0f));
			//Use model matrix to center the model to the world's origin
			model = glm::translate(model, glm::vec3((float)imgExt.GetImageWidth() / -2.0f, (float)imgExt.GetImageHeight() / 2.0f, 0.0f));

			glm::vec3 up(0.0f, 1.0f, 0.0f);
			glm::vec3 camPos(0.0f, 0.0f, 200.0f);
			
			//Calculate the vector pointing towards the current camera angle using trigonometric identities with the yaw and the pitch (X rotation = picth, Y rotation = yaw)
			glm::vec3 destVect(cos(glm::radians(InputManager::GetCurrentYCameraRotationModifier())) * cos(glm::radians(InputManager::GetCurrentXCameraRotationModifier())),
				sin(glm::radians(InputManager::GetCurrentXCameraRotationModifier())),
				sin(glm::radians(InputManager::GetCurrentYCameraRotationModifier())) * cos(glm::radians(InputManager::GetCurrentXCameraRotationModifier())));
			destVect = glm::normalize(destVect);

			//Move the camera left/right by adding a perpendicular vector to the up and orientation vector
			camPos -= glm::normalize(glm::cross(destVect, up)) * InputManager::GetCurrentXPosModifier();
			//Move the camera up/down. By doing a double cross product, I always find the proper vector going down regardless of the current orientation
			camPos -= glm::normalize(glm::cross(glm::cross(destVect, up), destVect)) * InputManager::GetCurrentYPosModifier();

			//Brings the camera in/out of the scene. (in/out being that it follows the orientation vector
			camPos += InputManager::GetCurrentZPosModifier() * destVect;

			view = glm::lookAt(camPos,
								camPos + destVect,
								up);

			//Apply translation and rotation to the view matrix,
			/*view = glm::rotate(view, glm::radians(InputManager::GetCurrentXCameraRotationModifier()), glm::vec3(1.0f, 0.0f, 0.0f));
			view = glm::rotate(view, glm::radians(InputManager::GetCurrentYCameraRotationModifier()), glm::vec3(0.0f, 1.0f, 0.0f));
			view = glm::rotate(view, glm::radians(InputManager::GetCurrentZCameraRotationModifier()), glm::vec3(0.0f, 0.0f, 1.0f));
			view = glm::translate(view, glm::vec3(InputManager::GetCurrentXPosModifier(), InputManager::GetCurrentYPosModifier(), InputManager::GetCurrentZPosModifier()));*/


			//Only (re)generate the perspective matrix if needed (on screen resize). 
			if (InputManager::GetAndClearShouldUpdatePErpectiveMatrix()) {
				projection = glm::perspective(45.0f, (GLfloat)InputManager::GetCurrentWindowWidth() / (GLfloat)InputManager::GetCurrentWindowHeight(), 0.1f, 1000.0f);
			}

			//calculate mvp matrix
			glm::mat4 mvp;
			mvp = projection * view * model;

			//Send the mvp matrix to OpenGL
			GLint mvpPos = glGetUniformLocation(s.GetCurrentProgram(), "mvp");
			glUniformMatrix4fv(mvpPos, 1, GL_FALSE, glm::value_ptr(mvp));


			imgExt.DrawImage();

			// Swap the screen buffers
			glfwSwapBuffers(window);
		}
	}

	//Buffers should get deletes when the imgExt objects gets destroyed

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}
