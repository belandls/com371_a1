/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Manages Mouse, Keyboard and Window callbacks. Everything is static because of the callback function pointer type
*/
#pragma once
#include "glfw3.h"

enum DisplayMode{
	Flat,
	Dots,
	Wireframe
};

class InputManager
{
private:
	static float _x_pos_modifier;
	static float _y_pos_modifier;
	static float _z_pos_modifier;
	static float _x_mesh_rotation_modifier;
	static float _y_mesh_rotation_modifier;
	static float _z_mesh_rotation_modifier; 
	static float _x_camera_rotation_modifier;
	static float _y_camera_rotation_modifier;
	static float _z_camera_rotation_modifier;
	static bool _zoom_activated;
	static bool _rotation_activated;
	static GLFWwindow* _current_window;
	static double _base_x; //base x position of the mouse (to calculate the difference when moved)
	static double _base_y; //base y position of the mouse (to calculate the difference when moved)
	static DisplayMode _current_display_mode;
	static bool _show_interpolated;
	static int _current_window_width;
	static int _current_window_height;
	static bool _should_update_perspective_mat;

	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	static void MousePositionCallback(GLFWwindow* window, double xpos, double ypos);
	static void WindowSizeCallback(GLFWwindow* window, int width, int height);

	InputManager();

public:

	static void AttachCallbacks(GLFWwindow* currentWindow);
	static float GetCurrentXPosModifier() { return _x_pos_modifier; };
	static float GetCurrentYPosModifier() { return _y_pos_modifier; };
	static float GetCurrentZPosModifier() { return _z_pos_modifier; };
	static float GetCurrentXMeshRotationModifier() { return _x_mesh_rotation_modifier; };
	static float GetCurrentYMeshRotationModifier() { return _y_mesh_rotation_modifier; };
	static float GetCurrentZMeshRotationModifier() { return _z_mesh_rotation_modifier; };
	static float GetCurrentXCameraRotationModifier() { return _x_camera_rotation_modifier; };
	static float GetCurrentYCameraRotationModifier() { return _y_camera_rotation_modifier; };
	static float GetCurrentZCameraRotationModifier() { return _z_camera_rotation_modifier; };
	static DisplayMode GetCurrentDisplayMode() { return _current_display_mode; }
	static int GetCurrentWindowHeight() { return _current_window_height; };
	static int GetCurrentWindowWidth() { return _current_window_width; };
	static bool ShouldShowInterpolated() { return _show_interpolated; };
	static void SetInitialWindowSize(GLFWwindow* currentWindow);
	static bool GetAndClearShouldUpdatePErpectiveMatrix();
};


