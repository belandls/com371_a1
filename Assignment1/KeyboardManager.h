#pragma once
#include "glfw3.h"
class KeyboardManager
{
private:

	static float _x_pos_modifier;
	static float _y_pos_modifier;
	static float _x_rotation_modifier;
	static float _y_rotation_modifier;
	static float _z_rotation_modifier;

	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

	KeyboardManager();
public:
	
	static void AttachCallbacks(GLFWwindow* currentWindow);
	static float GetCurrentXPosModifier() { return _x_pos_modifier; };
	static float GetCurrentYPosModifier() { return _y_pos_modifier; };
	static float GetCurrentXRotationModifier() { return _x_rotation_modifier; };
	static float GetCurrentYRotationModifier() { return _y_rotation_modifier; };
	static float GetCurrentZRotationModifier() { return _z_rotation_modifier; };
};

