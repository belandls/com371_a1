/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Extracts data from the heightmap image and the color image, generate the indexes for opengl and draws the image.
Will also calculate a 2X interpolation.
*/
#pragma once
#include <vector>
#include "glew.h"
#include <string>
class ImageDataExtractor
{
private:

	struct Point {
		float x;
		float y;
		float value;

		Point(float x, float y, float value) :x(x), y(y), value(value) {

		}
	};

	std::vector<GLfloat> _vertex_position_data;
	std::vector<GLfloat> _vertex_color_data;
	std::vector<GLuint> _vertex_index_data;
	std::vector<GLfloat> _interpolated_vertex_position_data;
	std::vector<GLfloat> _interpolated_vertex_color_data;
	std::vector<GLuint> _interpolated_vertex_index_data;
	int _img_height;
	int _img_width;
	bool _data_loaded;
	bool _showing_interpolated;

	GLuint _vao;
	GLuint _data_vbos[2];
	GLuint _ebo;

	void LoadOriginalData();
	void LoadInterpolatedData();

	Point Interpolate(Point dest, Point tl, Point tr, Point br, Point bl);
	template <class T>
	T** Initialize2DArray(int height, int width);
	template <class T>
	void Delete2DArray(T** destArray, int height);

public:

	ImageDataExtractor(std::string heighmapFilename, std::string colormapFilename);
	~ImageDataExtractor();

	void LoadDataInOpenGL();
	void DrawImage();

	int GetVertexPositionSize() { return _vertex_position_data.size() * sizeof(GLfloat); };
	int GetVertexColorSize() { return _vertex_color_data.size() * sizeof(GLfloat); };
	int GetVertexIndexSize() { return _vertex_index_data.size() * sizeof(GLuint); };
	int GetInterpolatedVertexPositionSize() { return _interpolated_vertex_position_data.size() * sizeof(GLfloat); };
	int GetInterpolatedVertexColorSize() { return _interpolated_vertex_color_data.size() * sizeof(GLfloat); };
	int GetInterpolatedVertexIndexSize() { return _interpolated_vertex_index_data.size() * sizeof(GLuint); };
	bool IsDataLoaded() { return _data_loaded; };
	int GetImageWidth() { return _img_width; };
	int GetImageHeight() { return _img_height; };
};

template<class T>
inline T ** ImageDataExtractor::Initialize2DArray(int height, int width)
{
	T** destArray = new T*[height];
	for (int i = 0; i < height; i++) {
		destArray[i] = new T[width];
	}
	return destArray;
}

template<class T>
inline void ImageDataExtractor::Delete2DArray(T ** destArray, int height)
{
	for (int i = 0; i < height; i++) {
		delete[] destArray[i];
	}
	delete[] destArray;
}
