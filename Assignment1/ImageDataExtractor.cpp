/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Extracts data from the heightmap image and the color image, generate the indexes for opengl and draws the image.
		 Will also calculate a 2X interpolation.
*/

#include "ImageDataExtractor.h"
#define cimg_use_jpeg
#include "CImg.h"
#include "InputManager.h"


//Load original image in OpenGL
void ImageDataExtractor::LoadOriginalData()
{
	glBindVertexArray(_vao);

	glBindBuffer(GL_ARRAY_BUFFER, _data_vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, GetVertexPositionSize(), _vertex_position_data.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, _data_vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, GetVertexColorSize(), _vertex_color_data.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, GetVertexIndexSize(), _vertex_index_data.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);
}

//Load the interpolated image in OpenGL
void ImageDataExtractor::LoadInterpolatedData()
{
	glBindVertexArray(_vao);

	glBindBuffer(GL_ARRAY_BUFFER, _data_vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, GetInterpolatedVertexPositionSize(), _interpolated_vertex_position_data.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, _data_vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, GetInterpolatedVertexColorSize(), _interpolated_vertex_color_data.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, GetInterpolatedVertexIndexSize(), _interpolated_vertex_index_data.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);
}

//Performs a Bilinear interpolation (based on 4 points)
ImageDataExtractor::Point ImageDataExtractor::Interpolate(Point dest, Point tl, Point tr, Point br, Point bl)
{
	float r1, r2;
	r1 = ((br.x - dest.x) / (br.x - bl.x)) * bl.value + ((dest.x - bl.x) / (br.x - bl.x)) * br.value;
	r2 = ((br.x - dest.x) / (br.x - bl.x)) * tl.value + ((dest.x - bl.x) / (br.x - bl.x)) * tr.value;
	dest.value = ((tl.y - dest.y) / (tl.y - bl.y)) * r1 + ((dest.y - bl.y) / (tl.y - bl.y)) * r2;
	return dest;
}

ImageDataExtractor::ImageDataExtractor(std::string heighmapFilename, std::string colormapFilename)
{
	_showing_interpolated = false;
	try {
		//Load the images in memory
		cimg_library::CImg<unsigned char> imgHm(heighmapFilename.c_str());
		cimg_library::CImg<unsigned char> imgHmGsData = imgHm.get_RGBtoYCbCr().get_channel(0); //Tranform the heightmap in YCbCr format and fet the luminance channel only
		cimg_library::CImg<unsigned char> imgColor(colormapFilename.c_str());

		_img_height = imgHm.height();
		_img_width = imgHm.width();

		//Initialize variables to store the interpolation data
		int newHeight = imgHm.height() * 2;
		int newWidth = imgHm.width() * 2;
		float** interpolatedZBuffer, **interpolatedRBuffer, **interpolatedGBuffer, **interpolatedBBuffer;
		interpolatedZBuffer = Initialize2DArray<float>(newHeight, newWidth);
		interpolatedRBuffer = Initialize2DArray<float>(newHeight, newWidth);
		interpolatedGBuffer = Initialize2DArray<float>(newHeight, newWidth);
		interpolatedBBuffer = Initialize2DArray<float>(newHeight, newWidth);

		for (int y = 0; y < imgHm.height(); y++) {
			for (int x = 0; x < imgHm.width(); x++) {

				//Generate the (x,y,z) coord. for vertex
				_vertex_position_data.push_back((GLfloat)x /*- imgHm.width() / 2.0f*/);
				_vertex_position_data.push_back((GLfloat)y * -1 /*- imgHm.height() / 2.0f*/);
				_vertex_position_data.push_back((GLfloat)(*imgHmGsData.data(x, y, 0, 0)) / 4.0f); //Divide by 4 to have a more natural height compared to the height/width ratio

				//Generate (R, G, B) color for vertex
				_vertex_color_data.push_back((GLfloat)(*imgColor.data(x, y, 0, 0)) / 255.0f);
				_vertex_color_data.push_back((GLfloat)(*imgColor.data(x, y, 0, 1)) / 255.0f);
				_vertex_color_data.push_back((GLfloat)(*imgColor.data(x, y, 0, 2)) / 255.0f);

				//Generate index for triangles
				//Follows this drawing scheme : Bottom Left -> Top Left(Current Vertex) -> Top Right, Top Right -> Bottom Right -> Bottom Left
				if (x < imgHm.width() - 1) {
					if (y < imgHm.height() - 1) {
						_vertex_index_data.push_back(((y + 1) * imgHm.width()) + x);
						_vertex_index_data.push_back(y * imgHm.width() + x);
						_vertex_index_data.push_back(y * imgHm.width() + x + 1);

						_vertex_index_data.push_back(y * imgHm.width() + x + 1);
						_vertex_index_data.push_back(((y + 1) * imgHm.width()) + x + 1);
						_vertex_index_data.push_back(((y + 1) * imgHm.width()) + x);
					}
				}

				//Variables used to extract the proper values from the image
				//To interpolate border pixels using bilinear interp., you still need 4 refenrence pixels, even though you don't have them all
				//So, when on the border (rightmost column and last row), the current pixel is repeated to create dumy reference points
				int trX, brX, brY, blY;
				if (x == imgHm.width() - 1) {
					trX = brX = x;
					brY = y;
				}
				else {
					trX = brX = x+1;
					brY = y + 1;
				}

				if (y == imgHm.height() - 1) {
					brY = blY = y;
				}
				else {
					blY = y + 1;
				}

				//Generate the 4 reference points for the height
				Point tl(x, y, *imgHmGsData.data(x, y, 0, 0) / 4.0f);
				Point tr(x + 1, y, *imgHmGsData.data(trX, y, 0, 0) / 4.0f);
				Point br(x + 1, y + 1, *imgHmGsData.data(brX, brY, 0, 0) / 4.0f);
				Point bl(x, y + 1, *imgHmGsData.data(x, blY, 0, 0) / 4.0f);

				//Generate the 4 reference points for the color(R channel)
				Point tlR(x, y, (GLfloat)(*imgColor.data(x, y, 0, 0)) / 255.0f);
				Point trR(x + 1, y, (GLfloat)(*imgColor.data(trX, y, 0, 0)) / 255.0f);
				Point brR(x + 1, y + 1, (GLfloat)(*imgColor.data(brX, brY, 0, 0)) / 255.0f);
				Point blR(x, y + 1, (GLfloat)(*imgColor.data(x, blY, 0, 0)) / 255.0f);

				//Generate the 4 reference points for the color(G channel)
				Point tlG(x, y, (GLfloat)(*imgColor.data(x, y, 0, 1)) / 255.0f);
				Point trG(x + 1, y, (GLfloat)(*imgColor.data(trX, y, 0, 1)) / 255.0f);
				Point brG(x + 1, y + 1, (GLfloat)(*imgColor.data(brX, brY, 0, 1)) / 255.0f);
				Point blG(x, y + 1, (GLfloat)(*imgColor.data(x, blY, 0, 1)) / 255.0f);

				//Generate the 4 reference points for the color(B channel)
				Point tlB(x, y, (GLfloat)(*imgColor.data(x, y, 0, 2)) / 255.0f);
				Point trB(x + 1, y, (GLfloat)(*imgColor.data(trX, y, 0, 2)) / 255.0f);
				Point brB(x + 1, y + 1, (GLfloat)(*imgColor.data(brX, brY, 0, 2)) / 255.0f);
				Point blB(x, y + 1, (GLfloat)(*imgColor.data(x, blY, 0, 2)) / 255.0f);

				//Generate destination pixels (to interpolate 2x, I create a pixel to the right, diagonal(under) and under the current pixel)
				Point destR(x + 0.5f, y, 0.0f);
				Point destD(x + 0.5f, y + 0.5f, 0.0f);
				Point destU(x, y + 0.5f, 0.0f);

				int interpolatedXindex = x * 2;
				int interpolatedYindex = y * 2;

				//Store the interpolated values in a 2D array for simplicity
				interpolatedZBuffer[interpolatedXindex][interpolatedYindex] = tl.value;
				interpolatedZBuffer[interpolatedXindex + 1][interpolatedYindex] = Interpolate(destR, tl, tr, br, bl).value;
				interpolatedZBuffer[interpolatedXindex + 1][interpolatedYindex + 1] = Interpolate(destD, tl, tr, br, bl).value;
				interpolatedZBuffer[interpolatedXindex][interpolatedYindex + 1] = Interpolate(destU, tl, tr, br, bl).value;

				interpolatedRBuffer[interpolatedXindex][interpolatedYindex] = tlR.value;
				interpolatedRBuffer[interpolatedXindex + 1][interpolatedYindex] = Interpolate(destR, tlR, trR, brR, blR).value;
				interpolatedRBuffer[interpolatedXindex + 1][interpolatedYindex + 1] = Interpolate(destD, tlR, trR, brR, blR).value;
				interpolatedRBuffer[interpolatedXindex][interpolatedYindex + 1] = Interpolate(destU, tlR, trR, brR, blR).value;

				interpolatedGBuffer[interpolatedXindex][interpolatedYindex] = tlG.value;
				interpolatedGBuffer[interpolatedXindex + 1][interpolatedYindex] = Interpolate(destR, tlG, trG, brG, blG).value;
				interpolatedGBuffer[interpolatedXindex + 1][interpolatedYindex + 1] = Interpolate(destD, tlG, trG, brG, blG).value;
				interpolatedGBuffer[interpolatedXindex][interpolatedYindex + 1] = Interpolate(destU, tlG, trG, brG, blG).value;

				interpolatedBBuffer[interpolatedXindex][interpolatedYindex] = tlB.value;
				interpolatedBBuffer[interpolatedXindex + 1][interpolatedYindex] = Interpolate(destR, tlB, trB, brB, blB).value;
				interpolatedBBuffer[interpolatedXindex + 1][interpolatedYindex + 1] = Interpolate(destD, tlB, trB, brB, blB).value;
				interpolatedBBuffer[interpolatedXindex][interpolatedYindex + 1] = Interpolate(destU, tlB, trB, brB, blB).value;

			}
		}

		//Convert the 2D arrays of interpolated array to a vertx for simplicity when sending to OpenGL
		for (int y = 0; y < newHeight; y++) {
			for (int x = 0; x < newWidth; x++) {
				// The '- imgHm.width() / 2.0f' centers the image and 'x/2' makes it so that the new pixels are created in between the pixels already present (otherwise, the image would 2x in size instead of becoming 'crisper')
				_interpolated_vertex_position_data.push_back((GLfloat)x/2 /*- imgHm.width() / 2.0f*/);
				_interpolated_vertex_position_data.push_back((GLfloat)y / 2 * -1.0f/* - imgHm.height() / 2.0f*/);
				_interpolated_vertex_position_data.push_back(interpolatedZBuffer[x][y]);

				_interpolated_vertex_color_data.push_back(interpolatedRBuffer[x][y]);
				_interpolated_vertex_color_data.push_back(interpolatedGBuffer[x][y]);
				_interpolated_vertex_color_data.push_back(interpolatedBBuffer[x][y]);

				//Sme index algorithm as the original image
				if (x < newWidth - 1) {
					if (y < newHeight - 1) {
						_interpolated_vertex_index_data.push_back(((y + 1) * newWidth) + x);
						_interpolated_vertex_index_data.push_back(y * newWidth + x);
						_interpolated_vertex_index_data.push_back(y * newWidth + x + 1);

						_interpolated_vertex_index_data.push_back(y * newWidth + x + 1);
						_interpolated_vertex_index_data.push_back(((y + 1) * newWidth) + x + 1);
						_interpolated_vertex_index_data.push_back(((y + 1) * newWidth) + x);
					}
				}
			}
		}
		//Delete the temp interpolated 2D arrays
		Delete2DArray(interpolatedZBuffer, newHeight);
		Delete2DArray(interpolatedRBuffer, newHeight);
		Delete2DArray(interpolatedGBuffer, newHeight);
		Delete2DArray(interpolatedBBuffer, newHeight);

	}
	catch (...) {
		_data_loaded = false;
	}
	_data_loaded = true;
}

//Creates the VAO, VBOs and EBO. Also send the original image to OpenGL
void ImageDataExtractor::LoadDataInOpenGL() {
	if (_data_loaded) {
		glGenVertexArrays(1, &_vao);
		glGenBuffers(2, _data_vbos);
		glGenBuffers(1, &_ebo);

		// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
		glBindVertexArray(_vao);

		glBindBuffer(GL_ARRAY_BUFFER, _data_vbos[0]);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, _data_vbos[1]);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(1);

		glBindVertexArray(0);

		LoadOriginalData();
	}

}

//Draws the image (original or interpolated depending on the InputManager::ShouldShowInterpolated() flag
void ImageDataExtractor::DrawImage() {

	//If the flag changes, send the proper data to OpenGL (only once)
	if (InputManager::ShouldShowInterpolated() && !_showing_interpolated) {
		LoadInterpolatedData();
		_showing_interpolated = true;
	}
	else if (!InputManager::ShouldShowInterpolated() && _showing_interpolated) {
		LoadOriginalData();
		_showing_interpolated = false;
	}

	glBindVertexArray(_vao);
	GLenum e;
	int size;
	if (InputManager::GetCurrentDisplayMode() == DisplayMode::Dots) {
		e = GL_POINTS;
	}
	else {
		e = GL_TRIANGLES;
	}
	if (_showing_interpolated) {
		size = _interpolated_vertex_index_data.size();
	}
	else {
		size = _vertex_index_data.size();
	}
	glDrawElements(e, size, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

ImageDataExtractor::~ImageDataExtractor()
{
	glDeleteVertexArrays(1, &_vao);
	glDeleteBuffers(2, _data_vbos);
	glDeleteBuffers(1, &_ebo);
}

