/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Manages Mouse, Keyboard and Window callbacks. Everything is static because of the callback function pointer type
*/

#include "InputManager.h"
#include <iostream>

bool InputManager::_zoom_activated;
bool InputManager::_rotation_activated;
GLFWwindow* InputManager::_current_window;
double InputManager::_base_x;
double InputManager::_base_y;
float InputManager::_x_pos_modifier;
float InputManager::_y_pos_modifier;
float InputManager::_z_pos_modifier;
float InputManager::_x_mesh_rotation_modifier;
float InputManager::_y_mesh_rotation_modifier;
float InputManager::_z_mesh_rotation_modifier;
float InputManager::_x_camera_rotation_modifier;
float InputManager::_y_camera_rotation_modifier;
float InputManager::_z_camera_rotation_modifier;
DisplayMode InputManager::_current_display_mode;
bool InputManager::_show_interpolated;
int InputManager::_current_window_width;
int InputManager::_current_window_height;
bool InputManager::_should_update_perspective_mat;

InputManager::InputManager()
{
}

//Attaches all the reuqired callbacks and intializes some of the static variables
void InputManager::AttachCallbacks(GLFWwindow * currentWindow)
{
	_should_update_perspective_mat = true;
	_x_pos_modifier = 0.0f;
	_y_pos_modifier = 0.0f;
	_z_pos_modifier = -100.0f;
	_x_mesh_rotation_modifier = 0.0f;
	_y_mesh_rotation_modifier = 0.0f;
	_z_mesh_rotation_modifier = 0.0f;
	_x_camera_rotation_modifier = 0.0f;
	_y_camera_rotation_modifier = -90.0f;
	_z_camera_rotation_modifier = 0.0f;
	glfwSetKeyCallback(currentWindow, InputManager::KeyCallback);
	glfwSetCursorPosCallback(currentWindow, InputManager::MousePositionCallback);
	glfwSetMouseButtonCallback(currentWindow, InputManager::MouseButtonCallback);
	glfwSetWindowSizeCallback(currentWindow, InputManager::WindowSizeCallback);
	_current_window = currentWindow;
}

void InputManager::MouseButtonCallback(GLFWwindow * window, int button, int action, int mods)
{
	//Depending on the mouse button pressed/released, activates/deactivates a flag to properly update modifiers during the MousePosition callback
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
		glfwGetCursorPos(_current_window, &_base_x, &_base_y);
		_zoom_activated = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE) {
		_zoom_activated = false;
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		glfwGetCursorPos(_current_window, &_base_x, &_base_y);
		_rotation_activated = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
		_rotation_activated = false;
	}

}

void InputManager::MousePositionCallback(GLFWwindow * window, double xpos, double ypos)
{
	//Update proper modifiers (depending on rotation/zoom) and take new position as base position to calculate the difference
	if (_zoom_activated) {
		_z_pos_modifier += ypos - _base_y;
		_base_x = xpos;
		_base_y = ypos;
	}
	if (_rotation_activated){
		_x_camera_rotation_modifier +=ypos - _base_y;
		_y_camera_rotation_modifier += xpos - _base_x;
		_base_x = xpos;
		_base_y = ypos;
	}
}

void InputManager::KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS || action == GLFW_REPEAT) {
		switch (key) {
			//Update Proper modifier depending on the key pressed
		case GLFW_KEY_UP:
			_y_pos_modifier -= 1.0f;
			break;
		case GLFW_KEY_LEFT:
			_x_pos_modifier += 1.0f;
			break;
		case GLFW_KEY_RIGHT:
			_x_pos_modifier -= 1.0f;
			break;
		case GLFW_KEY_DOWN:
			_y_pos_modifier += 1.0f;
			break;
		case GLFW_KEY_X:
			if (mods & GLFW_MOD_SHIFT){
				_x_mesh_rotation_modifier += 1.0f;
			}
			else{
				_x_mesh_rotation_modifier -= 1.0f;
			}
			break;
		case GLFW_KEY_Y:
			if (mods & GLFW_MOD_SHIFT){
				_y_mesh_rotation_modifier += 1.0f;
			}
			else{
				_y_mesh_rotation_modifier -= 1.0f;
			}
			break;
		case GLFW_KEY_Z:
			if (mods & GLFW_MOD_SHIFT){
				_z_mesh_rotation_modifier += 1.0f;
			}
			else{
				_z_mesh_rotation_modifier -= 1.0f;
			}
			break;
			//Update the view mode according to the key pressed
		case GLFW_KEY_P:
			if (mods & GLFW_MOD_SHIFT){
				if (_current_display_mode != DisplayMode::Dots){
					_current_display_mode = DisplayMode::Dots;
					std::cout << "Switching display mode to : Points" << std::endl;
				}
			}
			break;
		case GLFW_KEY_W:
			if (mods & GLFW_MOD_SHIFT){
				if (_current_display_mode != DisplayMode::Wireframe){
					glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
					_current_display_mode = DisplayMode::Wireframe;
					std::cout << "Switching display mode to : Wireframe" << std::endl;
				}
			}
			break;
		case GLFW_KEY_T:
			if (mods & GLFW_MOD_SHIFT){
				if (_current_display_mode != DisplayMode::Flat){
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					_current_display_mode = DisplayMode::Flat;
					std::cout << "Switching display mode to : Triangles" << std::endl;
				}
			}
			break;
		// 'I' is the added key to see or not the interpolated image
		case GLFW_KEY_I:
			if (mods & GLFW_MOD_SHIFT) {
				_show_interpolated = !_show_interpolated;
				std::cout << "Showing 2X interpolated data : " << _show_interpolated << std::endl;
			}
			break;
		}
	}
}

//Sets the viewport size in opengl and keep track of the initial window size
void InputManager::SetInitialWindowSize(GLFWwindow* currentWindow){
	glfwGetFramebufferSize(currentWindow, &_current_window_width, &_current_window_height);
	glViewport(0, 0, _current_window_width, _current_window_height);
}

void InputManager::WindowSizeCallback(GLFWwindow* window, int width, int height){
	_current_window_height = height;
	_current_window_width = width;
	glViewport(0, 0, _current_window_width, _current_window_height);
	_should_update_perspective_mat = true;
}

bool InputManager::GetAndClearShouldUpdatePErpectiveMatrix(){
	bool temp = _should_update_perspective_mat;
	_should_update_perspective_mat = false;
	return temp;
}